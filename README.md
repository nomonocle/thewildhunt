# The Wild Hunt - dotfiles #

* WM: [bspwm](https://github.com/baskerville/bspwm)
* GTK: [oomox](https://github.com/actionless/oomox)
* Fonts: [Termsyn](http://sourceforge.net/projects/termsyn), Ubuntu
* Icons: [Flattr](https://github.com/NitruxSA/flattr-icons)


### Screenshots ###

![1.jpg](https://bitbucket.org/repo/dog776/images/3030194691-1.jpg)

![2.jpg](https://bitbucket.org/repo/dog776/images/4034359846-2.jpg)

![3.jpg](https://bitbucket.org/repo/dog776/images/2816285459-3.jpg)